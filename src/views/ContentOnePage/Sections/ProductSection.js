import React from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

// @material-ui/icons
import Chat from "@material-ui/icons/Search";
import VerifiedUser from "@material-ui/icons/AssignmentInd";//Face
import Fingerprint from "@material-ui/icons/Spa";//PeopleAlt
// core components
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import InfoArea from "components/InfoArea/InfoArea.js";

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Divider from '@material-ui/core/Divider';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import { AccessAlarm, Healing } from '@material-ui/icons';

import styles from "assets/jss/material-kit-react/views/landingPageSections/productStyle.js";

const useStyles = makeStyles(styles);

export default function ProductSection() {
  const classes = useStyles();
  return (
    <div className={classes.section}>
      <GridContainer justify="center">
        <GridItem xs={12} sm={12} md={8}>
          <h2 className={classes.title}>Singular FONOAUDIOLOGÍA<br/> Clínica
Práctica Basada en Evidencia</h2>

          <div className={classes.description}>
              <p>
              Toda persona y condición de salud aunque puedan compartir características similares nunca son idénticamente iguales, y aún suponiendo que sean iguales entre una ocasión y otra puede haber ya aparecido un nuevo estudio, una nueva evidencia que permita mejorar el tratamiento o haya aparecido una persona con características muy similares y una respuesta adversa al tratamiento. {" "}
              </p>
              <p>
              Por ello para brindar el mejor tratamiento posible ya no es suficiente  proporcionar atención médica con base en los estudios y  la experiencia personal adquirida por años, sino una unión de :                
              </p>


          <List className={classes.root}>
      <ListItem alignItems="flex-start">

        <ListItemText
          primary="Evidencia"
          secondary={
            <React.Fragment>
              <Typography
                component="span"
                variant="body2"
                className={classes.inline}
                color="textPrimary"
              >
                
                <Healing /> Evidencia (Estudios, bases de datos, otros pacientes, Investigaciones)
              </Typography>
              {""}
            </React.Fragment>
          }
        />
      </ListItem>
      <Divider variant="inset" component="li" />
      <ListItem alignItems="flex-start">

        <ListItemText
          primary="Experiencia"
          secondary={
            <React.Fragment>
              <Typography
                component="span"
                variant="body2"
                className={classes.inline}
                color="textPrimary"
              >


               <Healing /> Experiencia Clinica (años de  experiencia del personal médico)
              </Typography>
              {""}
            </React.Fragment>
          }
        />
      </ListItem>
      <Divider variant="inset" component="li" />

      <ListItem alignItems="flex-start">
        <ListItemText
          primary="Paciente"
          secondary={
            <React.Fragment>
              <Typography
                component="span"
                variant="body2"
                className={classes.inline}
                color="textPrimary"
              >
                <Healing /> Particularidad, historia y contexto del paciente
              </Typography>
              {''}
            </React.Fragment>
          }
        />
      </ListItem>

    </List>
         


              <p>
              Hoy en día ya es posible ya se cuenta con cantidades de información, bases de datos,  estructuración de estudios de forma científica y accesible que permite integrarlos de una manera realista en el tratamiento.                
              </p>
              <p>
              Para ello se requiere un entrenamiento especializado  de carácter investigativo que permita recopilar de forma rápida enormes cantidades de información y con “ojo clínico” poder descartar, seleccionar y usar de manera adecuada los 3 pilares de la medicina moderna.
              </p>
              <p>
              A este conjunto de practicas se le conoce como medicina basada en la evidencia (MBE) 
              </p>
              <p>

              El concepto de o medicina basada en las pruebas en la Universidad de McMaster, en Canadá. Se define como el uso consciente, explícito, y dedicado del uso de  las mejores pruebas disponibles en la toma de decisiones sobre el cuidado del paciente teniendo en cuenta su historia, contexto, y particularidad física. individual. 

              </p>
              <p>
              La MBE, como indica su definición, no se basa solamente en la información disponible en la literatura científica (las “pruebas disponibles”) sino que requiere necesariamente del juicio o experiencia clínica para adecuar esa mejor evidencia externa disponible a la resolución de los problemas clínicos concretos de acuerdo a las características del paciente, sus preferencias y la disponibilidad de medios.
              </p>
              <p>
              La MBE supuso una revolución en la medicina al plantear que hay decisiones clínicas más correctas que otras. De esta forma redujo el énfasis en la experiencia clínica y en el razonamiento fisiopatológico y puso el acento en la evaluación crítica de la investigación. Ha permitido así una mejor integración de la evidencia disponible con las circunstancias y preferencias de los pacientes.

              </p>
          </div>
        </GridItem>
      </GridContainer>
      <div>

        <GridContainer>



          <GridItem xs={12} sm={12} md={4}>
            <InfoArea
              title="Mejores Diagnósticos"
              description="Diagnóstico e intervención individualizada orientada al
              mejor tratamiento posible de acuerdo a criterios clínicos
              y científicos."
              icon={Chat}
              iconColor="info"
              vertical
            />
          </GridItem>
          <GridItem xs={12} sm={12} md={4}>
            <InfoArea
              title="Mejores profesionales"
              description="Profesionales actualizados a la vanguardia de los
              nuevos enfoques de atención."
              icon={VerifiedUser}
              iconColor="success"
              vertical
            />
          </GridItem>
          <GridItem xs={12} sm={12} md={4}>
            <InfoArea
              title="InterConsulta"
              description="Referencia e interconsulta con servicios pediátricos y de
              adultos."
              icon={Fingerprint}
              iconColor="danger"
              vertical
            />
          </GridItem>
        </GridContainer>
      </div>
    </div>
  );
}
