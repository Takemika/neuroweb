import React from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

// @material-ui/icons
import Chat from "@material-ui/icons/Search";
import VerifiedUser from "@material-ui/icons/AssignmentInd";//Face
import Fingerprint from "@material-ui/icons/Spa";//PeopleAlt
// core components
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import InfoArea from "components/InfoArea/InfoArea.js";

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Divider from '@material-ui/core/Divider';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import { AccessAlarm, Healing } from '@material-ui/icons';

import styles from "assets/jss/material-kit-react/views/landingPageSections/productStyle.js";

const useStyles = makeStyles(styles);

export default function ProductSection() {
  const classes = useStyles();
  return (
    <div className={classes.section}>
      <GridContainer justify="center">
        <GridItem xs={12} sm={12} md={12}>
          <h2 className={classes.title}>Singular FONOAUDIOLOGÍA<br/> Clínica
Práctica Basada en Evidencia</h2>
          <h5 className={classes.description}>
          Diagnóstico e intervención individualizada orientada al
mejor tratamiento posible de acuerdo a criterios clínicos
y científicos. Incluimos  en nuestros tratamientos la mejor y más reciente evidencia de estudios científicos 
y los unimos con el contexto de la persona, su forma de vida, familia, historia. Miles de investigaciones 
nos dan las técnicas, herramientas más los tratamientos tienen que ser únicos individualizados como las personas
que tratamos. 
          </h5>
        </GridItem>
      </GridContainer>
      <div>

        <GridContainer>

          <GridItem xs={12} sm={12} md={12}>
          <List className={classes.root}>
      <ListItem alignItems="flex-start">

        <ListItemText
          primary="Brunch this weekend?"
          secondary={
            <React.Fragment>
              <Typography
                component="span"
                variant="body2"
                className={classes.inline}
                color="textPrimary"
              >
                
                <Healing /> Atención de la disfagia
              </Typography>
              {""}
            </React.Fragment>
          }
        />
      </ListItem>
      <Divider variant="inset" component="li" />
      <ListItem alignItems="flex-start">

        <ListItemText
          primary="Summer BBQ"
          secondary={
            <React.Fragment>
              <Typography
                component="span"
                variant="body2"
                className={classes.inline}
                color="textPrimary"
              >


               <Healing /> Atención de pacientes con deficiencias neurológicas
y neuro cognoscitivas.
              </Typography>
              {""}
            </React.Fragment>
          }
        />
      </ListItem>
      <Divider variant="inset" component="li" />

      <ListItem alignItems="flex-start">
        <ListItemText
          primary="Oui Oui"
          secondary={
            <React.Fragment>
              <Typography
                component="span"
                variant="body2"
                className={classes.inline}
                color="textPrimary"
              >
                <Healing /> Entrenamiento vocal para la voz patológica.
              </Typography>
              {''}
            </React.Fragment>
          }
        />
      </ListItem>
      <ListItem alignItems="flex-start">

        <ListItemText
          primary="Oui Oui"
          secondary={
            <React.Fragment>
              <Typography
                component="span"
                variant="body2"
                className={classes.inline}
                color="textPrimary"
              >
                <Healing /> Atención del paciente oncológico.
              </Typography>
              {''}
            </React.Fragment>
          }
        />
      </ListItem>      <ListItem alignItems="flex-start">

        <ListItemText
          primary="Oui Oui"
          secondary={
            <React.Fragment>
              <Typography
                component="span"
                variant="body2"
                className={classes.inline}
                color="textPrimary"
              >
                <Healing /> Atención de pacientes con deficiencias estructurales
y funcionales de cabeza y cuello.
              </Typography>
              {''}
            </React.Fragment>
          }
        />
      </ListItem>
      <ListItem alignItems="flex-start">
        <ListItemText
          primary="Oui Oui"
          secondary={
            <React.Fragment>
              <Typography
                component="span"
                variant="body2"
                className={classes.inline}
                color="textPrimary"
              >
<Healing /> Especialistas en atención y tratamiento efectivo de las
alteraciones de la deglución.
              </Typography>
              {''}
            </React.Fragment>
          }
        />
      </ListItem>
      <ListItem alignItems="flex-start">

        <ListItemText
          primary="Oui Oui"
          secondary={
            <React.Fragment>
              <Typography
                component="span"
                variant="body2"
                className={classes.inline}
                color="textPrimary"
              >
              <Healing />  Atención de pacientes con deficiencias estructurales
y funcionales de cabeza y cuello.
              </Typography>
              {''}
            </React.Fragment>
          }
        />
      </ListItem>
    </List>
          </GridItem>

          <GridItem xs={12} sm={12} md={4}>
            <InfoArea
              title="Mejores Diagnósticos"
              description="Diagnóstico e intervención individualizada orientada al
              mejor tratamiento posible de acuerdo a criterios clínicos
              y científicos."
              icon={Chat}
              iconColor="info"
              vertical
            />
          </GridItem>
          <GridItem xs={12} sm={12} md={4}>
            <InfoArea
              title="Mejores profesionales"
              description="Profesionales actualizados a la vanguardia de los
              nuevos enfoques de atención."
              icon={VerifiedUser}
              iconColor="success"
              vertical
            />
          </GridItem>
          <GridItem xs={12} sm={12} md={4}>
            <InfoArea
              title="InterConsulta"
              description="Referencia e interconsulta con servicios pediátricos y de
              adultos."
              icon={Fingerprint}
              iconColor="danger"
              vertical
            />
          </GridItem>
        </GridContainer>
      </div>
    </div>
  );
}
